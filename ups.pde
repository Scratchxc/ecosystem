void upBizz(){
  Iterator<Bizz> it = bizzArray.iterator();
  while(it.hasNext()){
    float distance=99999;
    Bizz b = it.next();
    b.show();
    b.move();
    if(b.spawnCooldown<=0){
      for(Bizz b2:bizzArray){
        if(b.pos.x != b2.pos.x && b.pos.y != b2.pos.y){
          if(PVector.dist(b.pos,b2.pos)<distance){
            b.attractTo(b2.pos.x,b2.pos.y);
            distance=PVector.dist(b.pos,b2.pos);
          }
          if(PVector.dist(b.pos,b2.pos)<5){
            bizzSpawn.add(new Bizz(b.pos.x, b.pos.y));
            b.spawnCooldown=400;
            b2.spawnCooldown=400;
          }
        }
      }
    }
    if(b.isDead()){
      it.remove();
    }
  }
  if(!bizzSpawn.isEmpty()){
    bizzArray.addAll(bizzSpawn);
    bizzSpawn.clear();
  }
}


void upPinger(){
  float distance=99999;
  for(Bizz b:bizzArray){
    if(PVector.dist(b.pos, pinger1.pos)<distance){
      pinger1.target=b.pos;
      distance=PVector.dist(b.pos, pinger1.pos);
    }
    if(PVector.dist(b.pos, pinger1.pos)<5){
      println(PVector.dist(b.pos, pinger1.pos));
      b.lifespan=1800;
    }
  }
  pinger1.show();
}


void upGravgrav(){
  gravgrav1.emit();
  gravgrav1.emit();
  gravgrav1.show();
  gravgrav1.moveToMouse();
}
