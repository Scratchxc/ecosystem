class AlgaBranch{
  PVector pos;
  PVector relativePos;
  AlgaBranch attachedTo;
  float vit=0;
  float acc=0;
  int lifespan=0;
  boolean last=true;
  boolean root=false;
  
  AlgaBranch(){
    last=true;
    root=false;
  }
  
  void makeRoot(PVector attachPos){
    root=false;
    pos=attachPos.copy();
  }
  
  
  void attachTo(AlgaBranch attachBranch){
    attachedTo=attachBranch;
    attachBranch.last=false;
    relativePos=PVector.random2D().mult(20);
    pos=attachBranch.pos.copy();
    pos.add(relativePos);
  }
  
  
  void move(){
    vit = map(noise(lifespan/1000),0,1,-2,2);
  }
  
}
