class ParticlePing{
  int frame=0;
  PVector pos;
  
  ParticlePing(float x, float y){
    pos = new PVector(x,y);
  }
  
  
  void show(){
    frame++;
    noFill();
    stroke(255,202-frame*2);
    strokeWeight(4);
    ellipseMode(RADIUS);
    ellipse(pos.x, pos.y,frame,frame);
    strokeWeight(1);
  }
  
  
  boolean isDead(){
    if(frame>200)
      return true;
    else
      return false;
  }
  
}
