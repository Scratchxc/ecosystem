class Gravgrav{
  PVector pos;
  PVector vit;
  PVector acc;
  ArrayList<ParticleGraviton> gravitons;
  int frame=0;
  float mass; //blackhole:10+; inker=5
  float maxspeed; //blackhole=4; inker = 7+
  
  Gravgrav(float x, float y, float m, float s){
    pos = new PVector(x,y);
    vit = new PVector(0,0);
    acc = new PVector(0,0);
    gravitons = new ArrayList<ParticleGraviton>();
    mass=m;
    maxspeed=s;
  }
  
  void show(){
    //move();
    noStroke();
    fill(0);
    //ellipse(pos.x,pos.y,5,5);
    updateGravitons();
    frame++;
  }
  
  void move(){
    vit.add(acc);
    pos.add(vit);
  }
  
  void moveToMouse(){
    PVector mouse = new PVector(mouseX/zoom,mouseY/zoom);
    PVector dir = PVector.sub(mouse,pos);
    dir.normalize();
    dir.mult((PVector.dist(mouse,pos))*0.01);
    
    acc=dir;
    vit.add(acc);
    vit.limit(maxspeed);
    pos.add(vit);
    
  }
  
  void emit(){
    PVector rnd = PVector.random2D();
    rnd.mult(random(1,4));
    gravitons.add(new ParticleGraviton(pos.x, pos.y, rnd.x, rnd.y));
  }
  
  void updateGravitons(){
    PVector gravitonAcc = new PVector();
    Iterator<ParticleGraviton> it = gravitons.iterator();
    while(it.hasNext()){
      ParticleGraviton graviton = it.next();
      gravitonAcc = PVector.sub(pos,graviton.pos);
      float dist = gravitonAcc.mag();
      dist = constrain(dist,15.0,25.0);
      gravitonAcc.normalize();
      gravitonAcc.mult(0.4*mass*graviton.mass/(sqrt(sqrt(dist))));
      graviton.applyForce(gravitonAcc);
      graviton.show();
      if(graviton.isDead()){
        it.remove();
      }
    }
  }
}
