class Pinger{
  ArrayList<ParticlePing> pings;
  PVector pos;
  PVector vit;
  PVector acc;
  int frame=0;
  int moveCount=0;
  int wait=60;
  PVector target;
  
  Pinger(float x, float y){
    pos = new PVector(x,y);
    vit = new PVector(0,0);
    acc = new PVector(0,0);
    target = new PVector(0,0);
    pings = new ArrayList<ParticlePing>();
  }
  
  
  void show(){
    emit();
    move();
    updatePings();
    frame++;
    moveCount++;
    
    //DEBUG
    //stroke(255,0,0);
    //noFill();
    //ellipse(target.x,target.y,20,20);
  }
  
  
  void emit(){
    if(frame>30-vit.mag()*10){
      pings.add(new ParticlePing(pos.x,pos.y));
      frame=0;
    }
  }
  
  
  void move(){
    vit.add(acc);
    pos.add(vit);
    //acc.sub(acc.x/20,acc.y/20);
    
    if(moveCount==wait){
      if(target.x==0 && target.y==0){
        vit.set(PVector.random2D());
      }
      else{
        vit.set(PVector.sub(target,pos).normalize());
      }
      vit.mult(random(1,3));
    }
    else if(moveCount>60+wait){
      vit.set(0,0);
      moveCount=0;
      wait=int(random(30,120));
    }
    
    if(pos.x<0)
      pos.x=width/zoom;
    if(pos.x>width/zoom)
      pos.x=0;
    if(pos.y<0)
      pos.y=height/zoom;
    if(pos.y>height/zoom)
      pos.y=0;
  }
  
  
  void updatePings(){
    Iterator<ParticlePing> it = pings.iterator();
    while(it.hasNext()){
      ParticlePing ping = it.next();
      ping.show();
      if(ping.isDead()){
        it.remove();
      }
    }
  }
}
