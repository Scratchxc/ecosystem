import java.util.*;

Snek snek1;
ArrayList<Bizz> bizzArray;
ArrayList<Bizz> bizzSpawn;
//Bizz bizz1;
Fizz fizz1;
Pinger pinger1;
Gravgrav gravgrav1;
int frame=0;
float zoom=0.5;

void setup(){
  frameRate(60);
  size(1600,900);
  snek1 = new Snek(300,300);
  bizzArray = new ArrayList<Bizz>();
  bizzSpawn = new ArrayList<Bizz>();
  bizzArray.add(new Bizz(350,400));
  bizzArray.add(new Bizz(600,650));
  bizzArray.add(new Bizz(200,700));
  bizzArray.add(new Bizz(600,200));
  fizz1 = new Fizz(450,400);
  pinger1 = new Pinger(500,500);
  gravgrav1 = new Gravgrav(400,400,5,7);
}

void draw(){
  background(200,210,255);
  pushMatrix();
  scale(zoom);
  snek1.show();
  snek1.moveToDir(map(noise(frame/100),0,1,0,360));
  upBizz();
  //fizz1.show();
  //fizz1.move();
  upPinger();
  upGravgrav();
  popMatrix();
  text(frameRate, 20,20);
  frame++;
}


void mousePressed(){
  
}
