class Bizz{
  PVector pos;
  PVector vit;
  PVector acc;
  PVector target;
  int lifespan=0;
  int spawnCooldown=300;
  
  Bizz(float x, float y){
    pos = new PVector(x,y);
    vit = new PVector(0,0);
    acc = new PVector(0,0);
    target = new PVector(0,0);
  }
  
  void show(){
    fill(0);
    noStroke();
    if(lifespan>1500)
      fill(100,0,0);
    ellipseMode(CENTER);
    ellipse(pos.x, pos.y, 5,5);
  }
  
  void move(){
    acc = PVector.random2D();
    
    if(target.x!=0 || target.y!=0){
      target.normalize();
      acc.add(target.mult(0.25));
    }
    
    vit.add(acc);
    float oldLim=0;
    if(lifespan>1500)
      oldLim=(lifespan-1500)/100;
    vit.limit(4-oldLim);
    pos.add(vit);
    
    if(pos.x<0)
      pos.x=width/zoom;
    if(pos.x>width/zoom)
      pos.x=0;
    if(pos.y<0)
      pos.y=height/zoom;
    if(pos.y>height/zoom)
      pos.y=0;
      
    lifespan++;
    spawnCooldown--;
  }
  
  void attractTo(float x, float y){
    target.set(x-pos.x,y-pos.y);
  }
  
  boolean isDead(){
    if(lifespan>1950){
      return true;
    }
    return false;
  }
}
