class ParticleGraviton{
  PVector pos;
  PVector vit;
  PVector acc;
  PVector force;
  float mass;
  int frame=0;
  
  ParticleGraviton(float x, float y, float vx, float vy){
    pos = new PVector(x,y);
    vit = new PVector(vx,vy);
    acc= new PVector(0,0);
    force = new PVector(0,0);
    mass = random(0.8,2.5);
  }
  
  void show(){
    move();
    noStroke();
    fill(0);
    ellipseMode(CENTER);
    ellipse(pos.x,pos.y,3,3);
  }
  
  void move(){
    vit.add(acc);
    vit.limit(6);
    pos.add(vit);
    frame++;
  }
  
  void setAcc(float ax,float ay){
    acc.set(ax,ay);
  }
  
  
  void applyForce(PVector f){
    force=f.copy();
    force.div(mass);
    acc.add(force);
  }
  
  
  boolean isDead(){
    if(frame>120)
      return true;
    else
      return false;
  }
  
}
