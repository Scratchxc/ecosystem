class Snek{
  PVector pos;
  PVector posBody1;
  PVector posBody2;
  PVector posBody3;
  PVector posBody4;
  PVector posTail;
  ArrayList<PVector> posTmp;
  PVector vit;
  PVector acc;
  PVector dir;
  int frame=0;
  float topspeed=5;
  
  Snek(float x, float y){
    pos = new PVector(x,y);
    posBody1 = new PVector(x,y);
    posBody2 = new PVector(x,y);
    posBody3 = new PVector(x,y);
    posBody4 = new PVector(x,y);
    posTail = new PVector(x,y);
    vit = new PVector(0,0);
    acc = new PVector(0,0);
    dir = new PVector(0,0);
    posTmp = new ArrayList<PVector>();
    for(int i=0;i<26;i++){
      posTmp.add(pos.copy());
    }
  }
  
  
  void show(){
    fill(30,255,80);
    stroke(0);
    strokeWeight(1);
    ellipseMode(CENTER);
    ellipse(posTail.x,posTail.y,15,15);
    ellipse(posBody4.x,posBody4.y,20,20);
    ellipse(posBody3.x,posBody3.y,25,25);
    ellipse(posBody2.x,posBody2.y,27,27);
    ellipse(posBody1.x,posBody1.y,23,23);
    ellipse(pos.x,pos.y,20,20);
    moveBodyParts();
    frame++;
  }
  
  
  void moveToMouse(){
    PVector mouse = new PVector(mouseX/zoom,mouseY/zoom);
    dir = PVector.sub(mouse,pos);
    dir.normalize();
    dir.mult(0.5);
    
    acc=dir;
    vit.add(acc);
    vit.limit(topspeed);
    pos.add(vit);
    pos.add(PVector.fromAngle(PVector.angleBetween(new PVector(1,0),dir)+HALF_PI).mult(cos(radians(frame*3))*3));
    
  }
  
  
  void moveToDir(float angle){
    dir = PVector.fromAngle(angle);
    dir.mult(0.5);
    
    acc=dir;
    vit.add(acc);
    vit.limit(topspeed);
    pos.add(vit);
    pos.add(PVector.fromAngle(PVector.angleBetween(new PVector(1,0),dir)+HALF_PI).mult(cos(radians(frame*3))*3));
    if(pos.x<0)
      pos.x=width/zoom;
    if(pos.x>width/zoom)
      pos.x=0;
    if(pos.y<0)
      pos.y=height/zoom;
    if(pos.y>height/zoom)
      pos.y=0;
    
  }
  
  void moveBodyParts(){
    posBody1.set(posTmp.get(5));
    posBody2.set(posTmp.get(10));
    posBody3.set(posTmp.get(15));
    posBody4.set(posTmp.get(20));
    posTail.set(posTmp.get(25));
    posTmp.get(0).set(pos);
    for(int i=25;i>0;i--){
      posTmp.get(i).set(posTmp.get(i-1));
    }
  } 
}
