class Slizzy{
  PVector pos;
  PVector posBody1;
  PVector posBody2;
  PVector posBody3;
  PVector posBody4;
  PVector posTail;
  ArrayList<PVector> posTmp;
  PVector vit;
  PVector acc;
  PVector dir;
  int frame=0;
  float topspeed=5;
  float incCos=0;
  
  Slizzy(float x, float y){
    pos = new PVector(x,y);
    posBody1 = new PVector(x,y);
    posBody2 = new PVector(x-5,y);
    posBody3 = new PVector(x-10,y);
    posBody4 = new PVector(x-15,y);
    posTail = new PVector(x-20,y);
    vit = new PVector(0,0);
    acc = new PVector(0,0);
    dir = new PVector(0,0);
    posTmp = new ArrayList<PVector>();
    for(int i=0;i<26;i++){
      posTmp.add(pos.copy());
    }
  }
  
  
  void show(){
    fill(25);
    stroke(0);
    strokeWeight(1);
    ellipseMode(CENTER);
    pushMatrix();
    rotate(PVector.angleBetween(new PVector(1,0),dir));
    translate(pos.x,pos.y);
    ellipse(posTail.x,posTail.y,15,15);
    ellipse(posBody4.x,posBody4.y,12,12);
    ellipse(posBody3.x,posBody3.y,12,12);
    ellipse(posBody2.x,posBody2.y,11,11);
    ellipse(-5,posBody1.y,11,11);
    ellipse(0,0,10,10);
    popMatrix();
    frame++;
  }
  
  
  void moveToMouse(){
    PVector mouse = new PVector(mouseX,mouseY);
    dir = PVector.sub(mouse,pos);
    dir.normalize();
    dir.mult(0.5);
    
    acc=dir;
    vit.add(acc);
    vit.limit(topspeed);
    pos.add(vit);
    pos.add(PVector.fromAngle(PVector.angleBetween(new PVector(1,0),dir)+HALF_PI).mult(cos(radians(frame*3))*3));
    
  }
  
  
  void moveToDir(float angle){
    dir = PVector.fromAngle(angle);
    dir.mult(0.5);
    
    acc=dir;
    vit.add(acc);
    vit.limit(topspeed);
    pos.add(vit);
    //pos.add(PVector.fromAngle(PVector.angleBetween(new PVector(1,0),dir)+HALF_PI).mult(cos(radians(frame*3))*3));
    if(pos.x<0)
      pos.x=width;
    if(pos.x>width)
      pos.x=0;
    if(pos.y<0)
      pos.y=height;
    if(pos.y>height)
      pos.y=0;
    
  }
  
  void moveBodyParts(){
    posBody1.set(posTmp.get(5));
    posBody2.set(posTmp.get(10));
    posBody3.set(posTmp.get(15));
    posBody4.set(posTmp.get(20));
    posTail.set(posTmp.get(25));
    posTmp.get(0).set(pos);
    for(int i=25;i>0;i--){
      posTmp.get(i).set(posTmp.get(i-1));
    }
  } 
}
