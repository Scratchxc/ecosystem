class Fizz{
  PVector pos;
  
  Fizz(int x, int y){
    pos = new PVector(x,y);
  }
  
  void show(){
    fill(0);
    rectMode(CENTER);
    rect(pos.x, pos.y, 5,5);
  }
  
  void move(){
    pos.add(PVector.random2D().mult(3));
  }
}
  
